package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/anacrolix/torrent"
)

type Args struct {
	Collector string
	Begin     int
	End       int
	Outputdir string
}

func main() {
	args := ParseArgs()

	fmt.Println("vim-go")
	fmt.Printf("Args: %v\n", args)
	// Download torrentfile
	torrentPath := fmt.Sprintf("%v/%v", args.Collector, "torrentfile/download")
	err := DownloadFile(torrentPath, "./torrentfile")
	if err != nil {
		fmt.Printf("Error %v\n", err)
		os.Exit(1)
	}
	fmt.Printf("Downloaded torrentfile @ %v\n", torrentPath)

	// Load torrentfile
	client, _ := torrent.NewClient(nil)
	defer client.Close()
	t, err := client.AddTorrentFromFile("./torrentfile")
	<-t.GotInfo()

	// Pieces have been provided in args
	if args.End == 0 {
		args.End = int(t.NumPieces())
	}

	// Download specific pieces
	fmt.Printf("Downloading piece %v - %v\n", args.Begin, args.End)
	t.DownloadPieces(args.Begin, args.End)

	// Upload piece by piece to the collector when piece id downloaded
	if args.Outputdir != "" {
		// Create output dir
		CreateDirIfNotExist(args.Outputdir)
		// piecesLeft := t.NumPieces()
		completedPieces := initCompletedPieces(args)
		log.Print("Completed pieces", completedPieces)
		for {
			// TODO Filter out completed pieces and check if we need to do another run
			piecesLeft := piecesLeftToUpload(completedPieces)
			if piecesLeft == 0 {
				log.Print("All pieces downloaded ", completedPieces)
				log.Print("Shutting down!")
				client.Close()
				os.Exit(0)
			} else {
				log.Print("Pieces still left ", piecesLeft)
			}

			for i := args.Begin; i < args.End; i++ {
				if completedPieces[i] {
					log.Print("Piece already completed", i)
				} else {
					missingBytes := t.PieceBytesMissing(i)
					if missingBytes > 0 {
						fmt.Printf("Missing bytes piece #%v: %v\n", i, missingBytes)
					} else {
						p := t.Peace(i)
						ip := p.Info()
						pl := ip.Length()
						r := io.NewSectionReader(p.Storage(), 0, pl)
						d, _ := os.Create(fmt.Sprintf("%v/%d", args.Outputdir, i))
						_, err := io.Copy(d, r)

						if err != nil {
							fmt.Printf("Err %v\n", err)
							os.Exit(1)
						}
						d.Close()
						fmt.Printf("Finished Piece #%v: %v\n", i, p.Storage().Completion().Complete)
						completedPieces[i] = true
						// Upload
						url := fmt.Sprintf("%v/pieces/%v", args.Collector, i)
						filepath := fmt.Sprintf("%v/%v", args.Outputdir, i)
						fmt.Printf("Uploading %v => %v", filepath, url)
						response := Upload(url, filepath, "piece")
						fmt.Printf("Uploaded Piece #%v\n%v\n", i, string(response))
						// fmt.Printf("%v pieces left\n", piecesLeft)
					}
				}
			}
			time.Sleep(100 * time.Millisecond)

		}
	} else {
		client.WaitAll()
	}
}

func initCompletedPieces(args Args) []bool {
	var completedPieces []bool
	for i := args.Begin; i < args.End; i++ {
		completedPieces = append(completedPieces, false)
	}
	return completedPieces
}

func piecesLeftToUpload(pieces []bool) int {
	amount := 0
	for _, piece := range pieces {
		if !piece {
			amount += 1
		}
	}
	return amount
}

func DownloadFile(url string, filepath string) error {
	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func ParseArgs() Args {
	collectorOption := flag.String("c", "", "collector")
	beginOption := flag.Int("b", 0, "Piece begin index")
	endOption := flag.Int("e", 0, "Piece end index")
	outputdirOption := flag.String("o", "output", "output dir for the binary files")
	flag.Parse()

	if *collectorOption == "" {
		flag.Usage()
		os.Exit(1)
	}

	return Args{*collectorOption, *beginOption, *endOption, *outputdirOption}
}

func Upload(url string, filename string, filetype string) []byte {
	file, err := os.Open(filename)

	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(filetype, filepath.Base(file.Name()))

	if err != nil {
		log.Fatal(err)
	}

	io.Copy(part, file)
	writer.Close()
	request, err := http.NewRequest("POST", url, body)

	if err != nil {
		log.Fatal(err)
	}

	request.Header.Add("Content-Type", writer.FormDataContentType())
	client := &http.Client{}

	response, err := client.Do(request)

	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	content, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Fatal(err)
	}

	return content
}

func CreateDirIfNotExist(dir string) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			panic(err)
		}
	}
}
