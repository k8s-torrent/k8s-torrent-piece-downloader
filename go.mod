module gitlab.com/tmestdagh/k8s-torrent-piece-downloader

go 1.13

require (
	github.com/anacrolix/torrent v1.9.0
	github.com/spaolacci/murmur3 v1.1.0 // indirect
)

replace github.com/anacrolix/torrent => ../../../github.com/anacrolix/torrent
